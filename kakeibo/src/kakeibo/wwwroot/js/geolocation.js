﻿
///////////////////位置情報取得/////////////////////
// 位置情報取得
var lat, lng, accuracy, isEnableGeoLocation = false;


// 位置情報取得成功時
function geolocationSuccess(position) {
    // 緯度
    lat = position.coords.latitude;
    // 経度
    lng = position.coords.longitude;
    // 緯度・経度誤差
    accuracy = position.coords.accuracy
    console.log(lat);
    console.log(lng);
    console.log(accuracy);
}

// 位置情報取得失敗時
function geolocationError(error) {
    var errorMessage = {
        0: "原因不明のエラーが発生しました…。",
        1: "位置情報の取得が許可されませんでした…。",
        2: "電波状況などで位置情報が取得できませんでした…。",
        3: "位置情報の取得に時間がかかり過ぎてタイムアウトしました…。",
    };

    console.log(errorMessage[error.code]);
}

// 位置情報取得オプション
var geoOptions = {
    "enableHighAccuracy": false,// true==より精度の高い位置情報を取得する
    "timeout": 8000,// タイムアウトまでの時間(ms)
    "maximumAge": 5000// 位置情報の有効期限(ms)
};

if (!navigator.geolocation) {
    isEnableGeoLocation = false;
    console.log('あなたの端末では、現在位置を取得できません。');
} else {
    isEnableGeoLocation = true;
    // 現在位置を取得する
    navigator.geolocation.getCurrentPosition(
      geolocationSuccess, geolocationError, geoOptions
    );
    console.log(lat);
    console.log(lng);

}

/////////////////////////////////////////////////////////////////