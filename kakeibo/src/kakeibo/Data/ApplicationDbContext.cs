﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using kakeibo.Models;
using kakeibo.Models.KakeiboModels;

namespace kakeibo.Data {
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser> {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options) {
        }

        protected override void OnModelCreating(ModelBuilder builder) {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
        public DbSet<Balance> Balance { get; set; }
        public DbSet<Coordinate> Coordinate { get; set; }
        public DbSet<Detail> Detail { get; set; }
        public DbSet<Kind> Kind { get; set; }
        public DbSet<Location> Location { get; set; }
        public DbSet<Photo> Photo { get; set; }
        public DbSet<PhotoGroup> PhotoGroup { get; set; }
        public DbSet<Payment> Payment { get; set; }
    }
}
