﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo {

    /// <summary>
    /// 定数クラス
    /// </summary>
    public class KkConst {

        #region <アカウント関連>

        /// <summary>
        /// 権限
        /// </summary>
        public class Role {

            /// <summary>
            /// 管理者
            /// </summary>
            public const string Admin = "管理者";

            /// <summary>
            /// 一般
            /// </summary>
            public const string Nomal = "一般";

            /// <summary>
            /// 権限名称一覧
            /// </summary>
            public static readonly List<string> RoleNameList = new List<string>() { Role.Admin, Role.Nomal };
        }



        /// <summary>
        /// 管理者ユーザテストアカウント
        /// </summary>
        public class TestAdminAccount {

            /// <summary>
            /// メールアドレス
            /// </summary>
            public const string Mail = "admin@mail.com";

            /// <summary>
            /// 名称
            /// </summary>
            public const string Name = "管理者テスト";

            /// <summary>
            /// パスワード
            /// </summary>
            public const string Password = "test";

        }

        /// <summary>
        /// 一般ユーザテストアカウント
        /// </summary>
        public class TestNomalAccount {

            /// <summary>
            /// メールアドレス
            /// </summary>
            public const string Mail = "nomal@mail.com";

            /// <summary>
            /// 名称
            /// </summary>
            public const string Name = "一般テスト";

            /// <summary>
            /// パスワード
            /// </summary>
            public const string Password = "test";

        }

        #endregion



        /// <summary>
        /// 一行最大表示件数
        /// </summary>
        public const int ROW_MAX = 20;
    }
}
