﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using kakeibo.Data;

namespace kakeibo.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170219092216_db1.0")]
    partial class db10
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1");

            modelBuilder.Entity("kakeibo.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.Balance", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ApplicationUserId");

                    b.Property<long?>("CoordinateId");

                    b.Property<DateTime?>("Created");

                    b.Property<string>("Creater");

                    b.Property<DateTime>("Date_");

                    b.Property<long>("LocationId");

                    b.Property<string>("Memo");

                    b.Property<long>("PaymentId");

                    b.Property<long?>("PhotoGroupId");

                    b.Property<DateTime>("Time_");

                    b.Property<int>("Total");

                    b.Property<DateTime?>("Updated");

                    b.Property<string>("Updater");

                    b.HasKey("Id");

                    b.HasIndex("ApplicationUserId");

                    b.HasIndex("CoordinateId");

                    b.HasIndex("LocationId");

                    b.HasIndex("PaymentId");

                    b.HasIndex("PhotoGroupId");

                    b.ToTable("Balance");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.Coordinate", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("Accuracy");

                    b.Property<DateTime?>("Created");

                    b.Property<string>("Creater");

                    b.Property<double>("Lat");

                    b.Property<double>("Lng");

                    b.Property<DateTime?>("Updated");

                    b.Property<string>("Updater");

                    b.HasKey("Id");

                    b.ToTable("Coordinate");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.Detail", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("BalanceId");

                    b.Property<DateTime?>("Created");

                    b.Property<string>("Creater");

                    b.Property<long>("KindId");

                    b.Property<string>("Memo");

                    b.Property<int>("Money");

                    b.Property<long?>("PhotoGroupId");

                    b.Property<int>("Sort");

                    b.Property<DateTime?>("Updated");

                    b.Property<string>("Updater");

                    b.HasKey("Id");

                    b.HasIndex("BalanceId");

                    b.HasIndex("KindId");

                    b.HasIndex("PhotoGroupId");

                    b.ToTable("Detail");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.Kind", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ApplicationUserId");

                    b.Property<DateTime?>("Created");

                    b.Property<string>("Creater");

                    b.Property<long?>("KindId");

                    b.Property<string>("Memo");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Short")
                        .IsRequired();

                    b.Property<int>("Sort");

                    b.Property<DateTime?>("Updated");

                    b.Property<string>("Updater");

                    b.HasKey("Id");

                    b.HasIndex("ApplicationUserId");

                    b.HasIndex("KindId");

                    b.ToTable("Kind");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.Location", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ApplicationUserId");

                    b.Property<long?>("CoordinateId");

                    b.Property<DateTime?>("Created");

                    b.Property<string>("Creater");

                    b.Property<long?>("LocationId");

                    b.Property<string>("Memo");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<long?>("PhotoGroupId");

                    b.Property<string>("Short")
                        .IsRequired();

                    b.Property<int>("Sort");

                    b.Property<DateTime?>("Updated");

                    b.Property<string>("Updater");

                    b.HasKey("Id");

                    b.HasIndex("ApplicationUserId");

                    b.HasIndex("CoordinateId");

                    b.HasIndex("LocationId");

                    b.HasIndex("PhotoGroupId");

                    b.ToTable("Location");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.Payment", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ApplicationUserId");

                    b.Property<DateTime?>("Created");

                    b.Property<string>("Creater");

                    b.Property<string>("Memo");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Short")
                        .IsRequired();

                    b.Property<int>("Sort");

                    b.Property<DateTime?>("Updated");

                    b.Property<string>("Updater");

                    b.HasKey("Id");

                    b.HasIndex("ApplicationUserId");

                    b.ToTable("Payment");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.Photo", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("Created");

                    b.Property<string>("Creater");

                    b.Property<DateTime>("Date_");

                    b.Property<string>("Name");

                    b.Property<string>("Path_");

                    b.Property<long>("PhotoGroupId");

                    b.Property<int>("Sort");

                    b.Property<DateTime?>("Updated");

                    b.Property<string>("Updater");

                    b.HasKey("Id");

                    b.HasIndex("PhotoGroupId");

                    b.ToTable("Photo");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.PhotoGroup", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("Created");

                    b.Property<string>("Creater");

                    b.Property<string>("Name");

                    b.Property<DateTime?>("Updated");

                    b.Property<string>("Updater");

                    b.HasKey("Id");

                    b.ToTable("PhotoGroup");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.Balance", b =>
                {
                    b.HasOne("kakeibo.Models.ApplicationUser", "ApplicationUser")
                        .WithMany("Balances")
                        .HasForeignKey("ApplicationUserId");

                    b.HasOne("kakeibo.Models.KakeiboModels.Coordinate", "Coordinate")
                        .WithMany("Balances")
                        .HasForeignKey("CoordinateId");

                    b.HasOne("kakeibo.Models.KakeiboModels.Location", "Location")
                        .WithMany("Balance")
                        .HasForeignKey("LocationId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("kakeibo.Models.KakeiboModels.Payment", "Payment")
                        .WithMany("Balances")
                        .HasForeignKey("PaymentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("kakeibo.Models.KakeiboModels.PhotoGroup", "PhotoGroup")
                        .WithMany()
                        .HasForeignKey("PhotoGroupId");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.Detail", b =>
                {
                    b.HasOne("kakeibo.Models.KakeiboModels.Balance", "Balance")
                        .WithMany("Details")
                        .HasForeignKey("BalanceId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("kakeibo.Models.KakeiboModels.Kind", "Kind")
                        .WithMany("Details")
                        .HasForeignKey("KindId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("kakeibo.Models.KakeiboModels.PhotoGroup", "PhotoGroup")
                        .WithMany()
                        .HasForeignKey("PhotoGroupId");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.Kind", b =>
                {
                    b.HasOne("kakeibo.Models.ApplicationUser", "ApplicationUser")
                        .WithMany("Kinds")
                        .HasForeignKey("ApplicationUserId");

                    b.HasOne("kakeibo.Models.KakeiboModels.Kind", "KindParent")
                        .WithMany("KindChildren")
                        .HasForeignKey("KindId");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.Location", b =>
                {
                    b.HasOne("kakeibo.Models.ApplicationUser", "ApplicationUser")
                        .WithMany("Locations")
                        .HasForeignKey("ApplicationUserId");

                    b.HasOne("kakeibo.Models.KakeiboModels.Coordinate", "Coordinate")
                        .WithMany("Locations")
                        .HasForeignKey("CoordinateId");

                    b.HasOne("kakeibo.Models.KakeiboModels.Location", "LocationParent")
                        .WithMany("LocationChildren")
                        .HasForeignKey("LocationId");

                    b.HasOne("kakeibo.Models.KakeiboModels.PhotoGroup", "PhotoGroup")
                        .WithMany()
                        .HasForeignKey("PhotoGroupId");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.Payment", b =>
                {
                    b.HasOne("kakeibo.Models.ApplicationUser", "ApplicationUser")
                        .WithMany("Payments")
                        .HasForeignKey("ApplicationUserId");
                });

            modelBuilder.Entity("kakeibo.Models.KakeiboModels.Photo", b =>
                {
                    b.HasOne("kakeibo.Models.KakeiboModels.PhotoGroup", "PhotoGroup")
                        .WithMany("Photos")
                        .HasForeignKey("PhotoGroupId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("kakeibo.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("kakeibo.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("kakeibo.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
