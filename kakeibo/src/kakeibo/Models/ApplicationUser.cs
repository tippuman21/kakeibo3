﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using kakeibo.Models.KakeiboModels;
using Microsoft.EntityFrameworkCore;
using kakeibo.Data;

namespace kakeibo.Models {
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser {

        // マスタメンテへの情報にアクセスできるようにする
        public ICollection<Balance> Balances { get; set; }
        public ICollection<Location> Locations { get; set; }
        public ICollection<Payment> Payments { get; set; }
        public ICollection<Kind> Kinds { get; set; }

        /// <summary>
        /// 初期データ作成
        /// </summary>
        /// <param name="context"></param>
        public static void Initialize(ApplicationDbContext context) {
            // ユーザが空ならば
            var userStore = new UserStore<ApplicationUser>(context);
            //var managerStore = new UserManager<ApplicationUser>()

            if (context.Users.Count() == 0) {
                // 管理者用アカウントの作成
                ApplicationUser user = new ApplicationUser();
                user.Id = "62c372c0-b8d9-4dbd-b9d8-ecfb8c6e0e72";
                user.AccessFailedCount = 0;
                user.ConcurrencyStamp = "2229aba0-69fd-49b2-a5de-183407e686af";
                user.Email = "admin@mail.com";
                user.EmailConfirmed = false;
                user.LockoutEnabled = true;
                user.LockoutEnd = null;
                user.NormalizedEmail = "ADMIN@MAIL.COM";
                user.NormalizedUserName = "ADMIN@MAIL.COM";
                // test1234!A
                user.PasswordHash = "AQAAAAEAACcQAAAAEFWDLraPuCxR7Eb62EosPQAJ4OUEJx6oSpVJurYgtUjGpRp7gduuUBsYF2bJKFBjXQ==";
                user.PhoneNumber = null;
                user.PhoneNumberConfirmed = false;
                user.SecurityStamp = "aacb2bea-c639-41c6-b220-80b6e0bfc960";
                user.TwoFactorEnabled = false;
                user.UserName = "admin@mail.com";
                context.Add(user);
                context.SaveChangesAsync();
            }


            if (context.Roles.Count() == 0) {
                // 権限の作成
                var store = new RoleStore<IdentityRole>(context);
                store.CreateAsync(new IdentityRole(KkConst.Role.Admin));
                context.SaveChangesAsync();
            }


            if (context.UserRoles.Count() == 0) {
                // admin@mail.comへ権限の付与
                var admin = context.Users.FirstOrDefault(n => n.Email == KkConst.TestAdminAccount.Mail);

                var adminRole = context.Roles.FirstOrDefault(n => n.Name == KkConst.Role.Admin);

                if (admin != null) {
                    var aa = new IdentityUserRole<string>();
                    aa.UserId = admin.Id;
                    aa.RoleId = adminRole.Id;
                    context.UserRoles.Add(aa);
                }

                context.SaveChangesAsync();
            }
        }


    }
}

