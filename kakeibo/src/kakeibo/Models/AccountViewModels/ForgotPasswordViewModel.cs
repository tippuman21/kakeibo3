﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "{0}は必須です。")]
        [EmailAddress(ErrorMessage ="メールアドレスの形式ではありません。")]
        [Display(Name = "メール")]
        public string Email { get; set; }
    }
}
