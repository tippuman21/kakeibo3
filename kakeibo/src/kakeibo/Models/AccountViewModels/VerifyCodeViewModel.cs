﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo.Models.AccountViewModels
{
    public class VerifyCodeViewModel
    {
        [Required(ErrorMessage = "{0}は必須です。")]
        public string Provider { get; set; }

        [Required(ErrorMessage = "{0}は必須です。")]
        public string Code { get; set; }

        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
