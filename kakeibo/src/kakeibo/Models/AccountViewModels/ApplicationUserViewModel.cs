﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo.Models.AccountViewModels
{
    /// <summary>
    /// ユーザ一覧
    /// </summary>
    public class ApplicationUserViewModel {
        [Display(Name = "メール")]
        public string Mail { get; set; }
        [Display(Name = "権限")]
        public string Role { get; set; }
    }
}
