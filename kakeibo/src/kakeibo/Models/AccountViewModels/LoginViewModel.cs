﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "{0}は必須です。")]
        [EmailAddress(ErrorMessage ="メールアドレスの形式ではありません。")]
        [Display(Name = "メール")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0}は必須です。")]
        [DataType(DataType.Password)]
        [Display(Name = "パスワード")]
        public string Password { get; set; }

        [Display(Name = "ログインしたままにする")]
        public bool RememberMe { get; set; }
    }
}
