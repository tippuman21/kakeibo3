﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo.Models.KakeiboModels
{
    /// <summary>
    /// 明細
    /// </summary>
    public class Detail : ModelBase {
        public long Id { get; set; }

        public long BalanceId { get; set; }
        public Balance Balance { get; set; }

        public long? PhotoGroupId { get; set; }
        public PhotoGroup PhotoGroup { get; set; }

        public long KindId { get; set; }
        public Kind Kind { get; set; }

        public int Money { get; set; }

        public int Sort { get; set; }
        public string Memo { get; set; }
    }
}
