﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo.Models.KakeiboModels
{
    /// <summary>
    /// 画像
    /// </summary>
    public class Photo : ModelBase {
        public long Id { get; set; }

        public long PhotoGroupId { get; set; }
        public PhotoGroup PhotoGroup { get; set; }

        public string Name { get; set; }
        public DateTime Date_ { get; set; }
        public string Path_ { get; set; }
        public int Sort { get; set; }
    }
}
