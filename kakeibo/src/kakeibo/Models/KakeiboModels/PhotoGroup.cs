﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo.Models.KakeiboModels
{
    public class PhotoGroup : ModelBase {
        // TODO:必要によっては

        public long Id { get; set; }
        public string Name { get; set; }

        // 子写真群
        public ICollection<Photo> Photos { get; set; }
    }
}
