﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo.Models.KakeiboModels
{
    /// <summary>
    /// 座標
    /// </summary>
    public class Coordinate : ModelBase {
        public long Id { get; set; }

        /// <summary>
        /// 緯度
        /// </summary>
        public double Lat { get; set; }

        /// <summary>
        /// 経度
        /// </summary>
        public double Lng { get; set; }

        /// <summary>
        /// 誤差
        /// </summary>
        public double Accuracy { get; set; }

        public ICollection<Location> Locations { get; set; }
        public ICollection<Balance> Balances { get; set; }
    }
}
