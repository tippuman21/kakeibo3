﻿using kakeibo.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo.Models.KakeiboModels {
    /// <summary>
    /// 分類
    /// </summary>
    public class Kind : ModelBase {

        public long Id { get; set; }

        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        [Display(Name = "分類名")]
        [Required(ErrorMessage = "{0}は必須です。")]
        public string Name { get; set; }

        [Display(Name = "分類名(略)")]
        [Required(ErrorMessage = "{0}は必須です。")]
        public string Short { get; set; }

        // 親分類
        [Display(Name = "親分類")]
        public long? KindId { get; set; }
        public Kind KindParent { get; set; }

        // 子分類
        public ICollection<Kind> KindChildren { get; set; }

        public ICollection<Detail> Details { get; set; }

        [Display(Name = "並び順")]
        [Required(ErrorMessage = "{0}は必須です。")]
        public int Sort { get; set; }

        [Display(Name = "メモ")]
        public string Memo { get; set; }


        /// <summary>
        /// 初期データ作成
        /// </summary>
        /// <param name="context">コンテキスト</param>
        public static void Initialize(ApplicationDbContext context) {
            var t = context.Kind;
            if (t.Any() == false) {

                t.Add(new Kind() {Name= "食費",Short="食費",Sort=10 });
                t.Add(new Kind() { Name = "水道光熱費", Short = "水道光熱費", Sort = 20 });
                t.Add(new Kind() { Name = "通信費", Short = "通信費", Sort = 30 });
                t.Add(new Kind() { Name = "遊興費", Short = "遊興費", Sort = 40 });
                t.Add(new Kind() { Name = "交通費", Short = "交通費", Sort = 50 });
                t.Add(new Kind() { Name = "美容費", Short = "美容費", Sort = 60 });
                t.Add(new Kind() { Name = "医療費", Short = "医療費", Sort = 70 });
                t.Add(new Kind() { Name = "被服費", Short = "被服費", Sort = 80 });
                t.Add(new Kind() { Name = "生活雑貨・日用品", Short = "生活雑貨・日用品", Sort = 90 });
                t.Add(new Kind() { Name = "住宅費", Short = "住宅費", Sort = 100 });
                t.Add(new Kind() { Name = "保険", Short = "保険", Sort = 110 });
                t.Add(new Kind() { Name = "社会保険", Short = "社会保険", Sort = 120 });
                t.Add(new Kind() { Name = "税金", Short = "税金", Sort = 130 });
                t.Add(new Kind() { Name = "その他費用", Short = "その他費用", Sort = 140 });

                context.SaveChanges();
            }
        }
    }
}
