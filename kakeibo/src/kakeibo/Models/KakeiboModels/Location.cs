﻿using kakeibo.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo.Models.KakeiboModels {
    /// <summary>
    /// 場所
    /// </summary>
    public class Location : ModelBase {
        public long Id { get; set; }

        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public long? CoordinateId { get; set; }
        public Coordinate Coordinate { get; set; }

        public long? PhotoGroupId { get; set; }
        public PhotoGroup PhotoGroup { get; set; }

        [Display(Name = "購入場所")]
        [Required(ErrorMessage = "{0}は必須です。")]
        public string Name { get; set; }

        [Display(Name = "購入場所(略)")]
        [Required(ErrorMessage = "{0}は必須です。")]
        public string Short { get; set; }

        // 親場所
        [Display(Name = "親場所")]
        public long? LocationId { get; set; }
        public Location LocationParent { get; set; }

        // 子場所
        public ICollection<Location> LocationChildren { get; set; }

        public ICollection<Balance> Balance { get; set; }

        [Display(Name = "並び順")]
        [Required(ErrorMessage = "{0}は必須です。")]
        public int Sort { get; set; }

        [Display(Name = "メモ")]
        public string Memo { get; set; }


        /// <summary>
        /// 初期データ作成
        /// </summary>
        /// <param name="context">コンテキスト</param>
        public static void Initialize(ApplicationDbContext context) {
            var t = context.Location;

            if (t.Any() == false) {
                t.Add(new Location() { Name = "コンビニ", Short = "コンビニ", Sort = 10 });
                t.Add(new Location() { Name = "スーパー", Short = "スーパー", Sort = 20 });
                t.Add(new Location() { Name = "ネット", Short = "ネット", Sort = 30 });
                t.Add(new Location() { Name = "その他", Short = "その他", Sort = 100 });
                context.SaveChanges();

                var d1 = context.Location.FirstOrDefault(n => n.Name == "コンビニ");
                t.Add(new Location() { LocationId = d1.Id, Name = "セブンイレブン", Short = "セブン", Sort = 10 });
                t.Add(new Location() { LocationId = d1.Id, Name = "ローソン", Short = "ローソン", Sort = 20 });
                t.Add(new Location() { LocationId = d1.Id, Name = "ファミリーマート", Short = "ファミマ", Sort = 30 });

                var d2 = context.Location.FirstOrDefault(n => n.Name == "スーパー");
                t.Add(new Location() { LocationId = d2.Id, Name = "まるたか", Short = "まるたか", Sort = 10 });
                t.Add(new Location() { LocationId = d2.Id, Name = "ララコープ", Short = "ララコープ", Sort = 20 });

                var d3 = context.Location.FirstOrDefault(n => n.Name == "ネット");
                t.Add(new Location() { LocationId = d3.Id, Name = "Amazon", Short = "Amazon", Sort = 10 });
                t.Add(new Location() { LocationId = d3.Id, Name = "楽天", Short = "楽天", Sort = 20 });

                context.SaveChanges();

                var d31 = context.Location.FirstOrDefault(n => n.Name == "楽天");
                t.Add(new Location() { LocationId = d31.Id, Name = "テスト", Short = "テスト", Sort = 10 });
                context.SaveChanges();
            }

        }
    }
}
