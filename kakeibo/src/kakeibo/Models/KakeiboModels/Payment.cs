﻿using kakeibo.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo.Models.KakeiboModels {
    /// <summary>
    /// 決済方法
    /// </summary>
    public class Payment : ModelBase {
        public long Id { get; set; }

        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public ICollection<Balance> Balances { get; set; }

        [Display(Name = "決済方法")]
        [Required(ErrorMessage = "{0}は必須です。")]
        public string Name { get; set; }

        [Display(Name = "決済方法(略)")]
        [Required(ErrorMessage = "{0}は必須です。")]
        public string Short { get; set; }

        [Display(Name = "並び順")]
        [Required(ErrorMessage = "{0}は必須です。")]
        public int Sort { get; set; }

        [Display(Name = "メモ")]
        public string Memo { get; set; }

        /// <summary>
        /// 初期データ作成
        /// </summary>
        /// <param name="context">コンテキスト</param>
        public static void Initialize(ApplicationDbContext context) {
            var t = context.Payment;
            if (t.Any() == false) {
                t.Add(new Payment() { Name = "現金", Short = "現金", Sort = 10 });
                t.Add(new Payment() { Name = "クレジットカード", Short = "カード", Sort = 20 });
                context.SaveChanges();
            }
        }
    }
}
