﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo.Models.KakeiboModels
{
    /// <summary>
    /// 収支
    /// </summary>
    public class Balance: ModelBase {
        public long Id { get; set; }

        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public long PaymentId { get; set; }
        public Payment Payment { get; set; }
        public long LocationId { get; set; }
        public Location Location { get; set; }
        public long? CoordinateId { get; set; }
        public Coordinate Coordinate { get; set; }
        public long? PhotoGroupId { get; set; }
        public PhotoGroup PhotoGroup { get; set; }

        public ICollection<Detail> Details { get; set; }

        public DateTime Date_ { get; set; }
        public DateTime Time_ { get; set; }
        public int Total { get; set; }
        public string Memo { get; set; }
    }
}
