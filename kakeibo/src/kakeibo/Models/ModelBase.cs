﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo.Models {

    /// <summary>
    /// モデル共通情報定義
    /// </summary>
    public class ModelBase {

        /// <summary>
        /// 登録者
        /// </summary>
        public string Creater { get; set; }

        /// <summary>
        /// 登録日時
        /// </summary>
        [Display(Name = "登録日時")]
        public DateTime? Created { get; set; }

        /// <summary>
        /// 更新者
        /// </summary>
        public string Updater { get; set; }

        /// <summary>
        /// 更新日時
        /// </summary>
        [Display(Name = "更新日時")]
        public DateTime? Updated { get; set; }


        /// <summary>
        /// 共通情報変更
        /// </summary>
        /// <param name="updTime">登録・更新日時</param>
        /// <param name="userId">ユーザID</param>
        public void ChangeCommon(DateTime? updTime = null, string userId = null) {
            DateTime? now = null;
            if (updTime == null) {
                now = DateTime.Now;
            } else {
                now = updTime;
            }


            // 登録情報が未設定
            if (this.Created == null) {
                this.Created = now;
                this.Creater = userId;
            }

            // 更新情報を設定
            this.Updated = now;
            this.Updater = userId;
        }
    }
}
