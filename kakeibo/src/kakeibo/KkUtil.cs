﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kakeibo {

    /// <summary>
    /// 家計簿ユーティリティ
    /// </summary>
    public class KkUtil {

        #region <プロパティ>

        #region <プロパティ：Singleton>

        /// <summary>
        /// Instance
        /// </summary>
        private static KkUtil _instance = null;

        /// <summary>
        /// Instance
        /// </summary>
        public static KkUtil Instance {
            get {
                if (_instance == null) {
                    _instance = new KkUtil();
                }
                return _instance;
            }
        }

        #endregion

        #endregion



        #region <コンストラクタ>

        /// <summary>
        /// コンストラクタ
        /// </summary>
        private KkUtil() {

        }

        #endregion



        #region <メソッド>

        #region <メソッド：文字列制御>

        /// <summary>
        /// 文字列が空白かどうかを判定する
        /// </summary>
        /// <param name="value">検査対象文字列</param>
        /// <returns>true == 空白</returns>
        public bool IsBlank(string value) {
            if (string.IsNullOrWhiteSpace(value)) {
                return true;
            }
            return false;
        }

        #endregion


        /// <summary>
        /// 空要素を保持するセレクトリストを作成する
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public SelectList GenEmptySelectList(SelectList list) {
            return null;
        }

        #endregion
    }
}
