﻿using kakeibo.Models.KakeiboModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace kakeibo {
    /// <summary>
    /// 拡張メソッド
    /// </summary>
    public static class KkExtend {

        /// <summary>
        /// UserからユーザIDを取得できるようにする
        /// </summary>
        /// <param name="principal"></param>
        /// <returns>ユーザIDを</returns>
        public static string GetUserId(this IPrincipal principal) {
            var claimsIdentity = (ClaimsIdentity)principal.Identity;
            var claim = claimsIdentity.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier);
            if (claim != null) {
                return claim.Value;
            }
            return string.Empty;
        }



        /// <summary>
        /// アクセス可能なデータ一覧を取得する
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static IQueryable<Kind> UserFilter(this DbSet<Kind> model, ClaimsPrincipal user) {
            if (user.IsInRole(KkConst.Role.Admin)) {
                var tmp = model.OrderBy(n => n.Sort);
                return tmp;
            } else {
                var tmp = model.Where(n => (n.ApplicationUserId == user.GetUserId() || KkUtil.Instance.IsBlank(n.ApplicationUserId)));
                tmp = tmp.OrderBy(n => n.Sort);
                return tmp;
            }
        }



        /// <summary>
        /// 再帰構造を考慮した並び替えを行う
        /// </summary>
        /// <param name="list">並び替え</param>
        /// <returns></returns>
        public static List<Kind> SortEx(this List<Kind> list, long? parent = null) {
            List<Kind> resultList = new List<Kind>();
            var tmp = list.Where(n => n.KindId == parent).OrderBy(n => n.Sort).ToList();
            foreach (var item in tmp) {
                resultList.Add(item);
                resultList.AddRange(list.SortEx(item.Id));
            }

            return resultList;
        }




        /// <summary>
        /// アクセス可能なデータ一覧を取得する
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static IQueryable<Location> UserFilter(this DbSet<Location> model, ClaimsPrincipal user) {
            if (user.IsInRole(KkConst.Role.Admin)) {
                var tmp = model.OrderBy(n => n.Sort);
                return tmp;
            } else {
                var tmp = model.Where(n => (n.ApplicationUserId == user.GetUserId() || KkUtil.Instance.IsBlank(n.ApplicationUserId)));
                tmp = tmp.OrderBy(n => n.Sort);
                return tmp;
            }
        }




        /// <summary>
        /// 再帰構造を考慮した並び替えを行う
        /// </summary>
        /// <param name="list">並び替え</param>
        /// <returns></returns>
        public static List<Location> SortEx(this List<Location> list, long? parent = null) {
            List<Location> resultList = new List<Location>();
            var tmp = list.Where(n => n.LocationId == parent).OrderBy(n => n.Sort).ToList();
            foreach (var item in tmp) {
                resultList.Add(item);
                resultList.AddRange(list.SortEx(item.Id));
            }

            return resultList;
        }




        /// <summary>
        /// アクセス可能なデータ一覧を取得する
        /// </summary>
        /// <param name="model"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static IQueryable<Payment> UserFilter(this DbSet<Payment> model, ClaimsPrincipal user) {
            if (user.IsInRole(KkConst.Role.Admin)) {
                var tmp = model.OrderBy(n => n.Sort);
                return tmp;
            } else {
                var tmp = model.Where(n => (n.ApplicationUserId == user.GetUserId() || KkUtil.Instance.IsBlank(n.ApplicationUserId)));
                tmp = tmp.OrderBy(n => n.Sort);
                return tmp;
            }
        }
    }
}
