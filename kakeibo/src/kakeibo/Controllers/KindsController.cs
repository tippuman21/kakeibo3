﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using kakeibo.Data;
using kakeibo.Models.KakeiboModels;

namespace kakeibo.Controllers {
    public class KindsController : Controller {
        private readonly ApplicationDbContext _context;

        public KindsController(ApplicationDbContext context) {
            _context = context;
        }

        // GET: Kinds
        public async Task<IActionResult> Index() {
            var applicationDbContext = _context.Kind.UserFilter(this.User).Include(k => k.ApplicationUser).Include(k => k.KindParent);
            return View(applicationDbContext.ToList().SortEx());
        }


        // GET: Kinds/Create
        public IActionResult Create() {
            ViewData["KindId"] = new SelectList(_context.Kind.UserFilter(this.User).ToList().SortEx(), "Id", "Short");
            return View();
        }

        // POST: Kinds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ApplicationUserId,KindId,Memo,Name,Short,Sort")] Kind kind) {
            if (ModelState.IsValid) {
                if (!this.User.IsInRole(KkConst.Role.Admin)) {
                    kind.ApplicationUserId = this.User.GetUserId();
                }
                // TODO:循環参照検知
                _context.Add(kind);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["KindId"] = new SelectList(_context.Kind.UserFilter(this.User).ToList().SortEx(), "Id", "Short", kind.KindId);
            return View(kind);
        }

        // GET: Kinds/Edit/5DDS
        public async Task<IActionResult> Edit(long? id) {
            if (id == null) {
                return NotFound();
            }

            var kind = await _context.Kind.UserFilter(this.User).SingleOrDefaultAsync(m => m.Id == id);
            if (kind == null) {
                return NotFound();
            }
            ViewData["KindId"] = new SelectList(_context.Kind.UserFilter(this.User).ToList().SortEx(), "Id", "Short", kind.KindId);
            return View(kind);
        }

        // POST: Kinds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,ApplicationUserId,KindId,Memo,Name,Short,Sort")] Kind kind) {
            if (id != kind.Id) {
                return NotFound();
            }

            if (ModelState.IsValid) {
                try {
                    _context.Update(kind);
                    await _context.SaveChangesAsync();
                } catch (DbUpdateConcurrencyException) {
                    if (!KindExists(kind.Id)) {
                        return NotFound();
                    } else {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["KindId"] = new SelectList(_context.Kind.UserFilter(this.User).ToList().SortEx(), "Id", "Short", kind.KindId);
            return View(kind);
        }

        // GET: Kinds/Delete/5
        public async Task<IActionResult> Delete(long? id) {
            if (id == null) {
                return NotFound();
            }
            var kind = await _context.Kind.UserFilter(this.User).SingleOrDefaultAsync(m => m.Id == id);
            _context.Kind.Remove(kind);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool KindExists(long id) {
            return _context.Kind.UserFilter(this.User).Any(e => e.Id == id);
        }
    }
}
