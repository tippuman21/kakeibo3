using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using kakeibo.Data;
using kakeibo.Models.KakeiboModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace kakeibo.Controllers {
    public class PaymentsController : Controller {
        private readonly ApplicationDbContext _context;

        public PaymentsController(ApplicationDbContext context) {
            _context = context;
        }

        // GET: Payments
        public async Task<IActionResult> Index() {
            var applicationDbContext = _context.Payment.UserFilter(this.User).Include(p => p.ApplicationUser);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Payments/Create
        public IActionResult Create() {
            return View();
        }

        // POST: Payments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Memo,Name,Short,Sort")] Payment payment) {
            if (ModelState.IsValid) {
                if (!this.User.IsInRole(KkConst.Role.Admin)) {
                    payment.ApplicationUserId = this.User.GetUserId();
                }
                _context.Add(payment);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(payment);
        }

        // GET: Payments/Edit/5
        public async Task<IActionResult> Edit(long? id) {
            if (id == null) {
                return NotFound();
            }

            var payment = await _context.Payment.UserFilter(this.User).SingleOrDefaultAsync(m => m.Id == id);
            if (payment == null) {
                return NotFound();
            }
            return View(payment);
        }

        // POST: Payments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,ApplicationUserId,Memo,Name,Short,Sort")] Payment payment) {
            if (id != payment.Id) {
                return NotFound();
            }

            if (ModelState.IsValid) {
                try {
                    _context.Update(payment);
                    await _context.SaveChangesAsync();
                } catch (DbUpdateConcurrencyException) {
                    if (!PaymentExists(payment.Id)) {
                        return NotFound();
                    } else {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(payment);
        }

        // GET: Payments/Delete/5
        public async Task<IActionResult> Delete(long? id) {
            if (id == null) {
                return NotFound();
            }
            var payment = await _context.Payment.UserFilter(this.User).SingleOrDefaultAsync(m => m.Id == id);
            _context.Payment.Remove(payment);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PaymentExists(long id) {
            return _context.Payment.UserFilter(this.User).Any(e => e.Id == id);
        }
    }
}
