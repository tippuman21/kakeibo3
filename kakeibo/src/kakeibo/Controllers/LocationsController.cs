using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using kakeibo.Data;
using kakeibo.Models.KakeiboModels;

namespace kakeibo.Controllers
{
    public class LocationsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LocationsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Locations
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Location.UserFilter(this.User).Include(l => l.ApplicationUser).Include(l => l.Coordinate).Include(l => l.LocationParent).Include(l => l.PhotoGroup);
            return View(applicationDbContext.ToList().SortEx());
        }


        // GET: Locations/Create
        public IActionResult Create()
        {
            ViewData["CoordinateId"] = new SelectList(_context.Coordinate, "Id", "Id");
            ViewData["LocationId"] = new SelectList(_context.Location.UserFilter(this.User).ToList().SortEx(), "Id", "Short");
            ViewData["PhotoGroupId"] = new SelectList(_context.PhotoGroup, "Id", "Id");
            return View();
        }

        // POST: Locations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ApplicationUserId,CoordinateId,LocationId,Memo,Name,PhotoGroupId,Short,Sort")] Location location)
        {
            if (ModelState.IsValid)
            {
                if (!this.User.IsInRole(KkConst.Role.Admin)) {
                    location.ApplicationUserId = this.User.GetUserId();
                }
                _context.Add(location);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["CoordinateId"] = new SelectList(_context.Coordinate, "Id", "Id", location.CoordinateId);
            ViewData["LocationId"] = new SelectList(_context.Location.UserFilter(this.User).ToList().SortEx(), "Id", "Short", location.LocationId);
            ViewData["PhotoGroupId"] = new SelectList(_context.PhotoGroup, "Id", "Id", location.PhotoGroupId);
            return View(location);
        }

        // GET: Locations/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var location = await _context.Location.UserFilter(this.User).SingleOrDefaultAsync(m => m.Id == id);
            if (location == null)
            {
                return NotFound();
            }
            ViewData["CoordinateId"] = new SelectList(_context.Coordinate, "Id", "Id", location.CoordinateId);
            ViewData["LocationId"] = new SelectList(_context.Location.UserFilter(this.User).ToList().SortEx(), "Id", "Short", location.LocationId);
            ViewData["PhotoGroupId"] = new SelectList(_context.PhotoGroup, "Id", "Id", location.PhotoGroupId);
            return View(location);
        }

        // POST: Locations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Id,ApplicationUserId,CoordinateId,LocationId,Memo,Name,PhotoGroupId,Short,Sort")] Location location)
        {
            if (id != location.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(location);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LocationExists(location.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["CoordinateId"] = new SelectList(_context.Coordinate, "Id", "Id", location.CoordinateId);
            ViewData["LocationId"] = new SelectList(_context.Location.UserFilter(this.User).ToList().SortEx(), "Id", "Short", location.LocationId);
            ViewData["PhotoGroupId"] = new SelectList(_context.PhotoGroup, "Id", "Id", location.PhotoGroupId);
            return View(location);
        }

        // GET: Locations/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null) {
                return NotFound();
            }
            var location = await _context.Location.UserFilter(this.User).SingleOrDefaultAsync(m => m.Id == id);
            _context.Location.Remove(location);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool LocationExists(long id)
        {
            return _context.Location.UserFilter(this.User).Any(e => e.Id == id);
        }
    }
}
